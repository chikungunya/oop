<?php

    require('animal.php');
    $animal = new Animal("Shaun");
    echo "Name : ". $animal->name ."<br>";
    echo "legs : ". $animal->Legs ."<br>";
    echo "cold blooded : ".$animal->Cold_blooded ."<br><br>";

    require('frog.php');
    $frog = new Frog("buduk");
    echo "Name : ". $frog->name ."<br>";
    echo "legs : ". $frog->Legs ."<br>";
    echo "cold blooded : ". $frog->Cold_blooded ."<br>";
    echo "Jump : ". $frog->Jump ."<br><br>";

    require('ape.php');
    $ape = new Ape("kera sakti");
    echo "Name : ". $ape->name ."<br>";
    echo "legs : ". $ape->Legs ."<br>";
    echo "cold blooded : ". $ape->Cold_blooded ."<br>";
    echo "Yell : ". $ape->Yell ."<br><br>";

    ?>